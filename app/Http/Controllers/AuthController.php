<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }

    public function welcome(Request $request) {
        return view('welcome');
    }

    public function welcome_post(Request $request) {
        //dd($request->all());
        $firstName = $request ["FirstName"];
        $lastName = $request ["LastName"];
        return view('welcome', compact('firstName', 'lastName'));
    }
}